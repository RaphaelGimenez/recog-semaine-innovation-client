const HOST_URI = process.env.REACT_APP_API_URI

const RECORDINGS_ENDPOINT = "recordings"

const FILES_ENDPOINT = {
  base: "files",
  download: `files/download`,
}

const PATHOLOGIES_ENDPOINT = "pathologies"

export { HOST_URI, RECORDINGS_ENDPOINT, FILES_ENDPOINT, PATHOLOGIES_ENDPOINT }
