import React from "react"
import PropTypes from "prop-types"

const MenuIcon = ({ width, height, className }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
    className={className}
  >
    <path d="M3 12h18M3 6h18M3 18h18" />
  </svg>
)

MenuIcon.defaultProps = {
  width: 24,
  height: 24,
  className: "",
}

MenuIcon.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  className: PropTypes.string,
}

export default MenuIcon
