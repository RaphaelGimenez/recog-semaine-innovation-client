import React from "react"
import PropTypes from "prop-types"

const PlayIcon = ({ width, height, className, label }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
    className={className}
    aria-label={label}
  >
    <path d="M5 3l14 9-14 9V3z" />
  </svg>
)

PlayIcon.defaultProps = {
  width: 24,
  height: 24,
  className: "",
  label: "Stop icon",
}

PlayIcon.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  className: PropTypes.string,
  label: PropTypes.string,
}

export default PlayIcon
