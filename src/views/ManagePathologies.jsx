import React from "react"
import SearchBar from "../components/SearchBar"
import useDebounce from "../hooks/useDebounce"
import PlusIcon from "../icons/PlusIcon"
import Pathology from "../components/Pathology"
import { HOST_URI, PATHOLOGIES_ENDPOINT } from "../env/environnement"

const ManagePathologies = () => {
  const [searchTerm, setSearchTerm] = React.useState("")
  const [pathologies, setPathologies] = React.useState([])
  const [newPathology, setNewPathology] = React.useState("")
  const debouncedSearchTerm = useDebounce(searchTerm, 300)
  const PATHOLOGIES_URL = `${HOST_URI}/${PATHOLOGIES_ENDPOINT}`

  React.useEffect(() => {
    const searchUrl = new URL(PATHOLOGIES_URL)
    if (searchTerm) {
      searchUrl.searchParams.append("name", searchTerm)
    }
    fetch(searchUrl, {
      method: "get",
    })
      .then(resp => resp.json())
      .then(res => {
        setPathologies(res)
        if (!res.length) {
          setNewPathology(searchTerm)
        }
      })
  }, [debouncedSearchTerm])

  const onSearchChange = e => {
    const { value } = e.target
    setSearchTerm(value)
  }

  const onNewPathologyChange = e => {
    const { value } = e.target
    setNewPathology(value)
  }

  const onNewPathologyCreate = () => {
    if (newPathology) {
      fetch(new URL(PATHOLOGIES_URL), {
        method: "post",
        body: JSON.stringify({ name: newPathology, valid: true }),
        headers: { "Content-Type": "application/json" },
      })
        .then(resp => resp.json())
        .then(res => {
          setNewPathology("")
          setPathologies([...pathologies, res])
        })
    }
  }

  const onPathologyRemove = id => {
    fetch(new URL(`${PATHOLOGIES_URL}/${id}`), {
      method: "delete",
    })
      .then(resp => resp.json())
      .then(res => {
        if (res.status === "success") {
          setPathologies(pathologies.filter(pathology => pathology._id !== id))
        }
      })
  }

  return (
    <div>
      <h1 className="text-white text-2xl p-4 pb-0 md:text-4xl md:p-12 md:pb-0 font-bold">
        Gérer les pathologies
      </h1>
      <div className="mb-4 md:mb-8 p-4 md:p-12 md:pb-0 pb-0">
        <SearchBar
          className="mb-4 md:mb-12 max-w-sm"
          label="Rechercher une pathologie"
          placeholder="Rechercher une pathologie"
          onChange={onSearchChange}
        />
      </div>
      <div className="h-full overflow-y-scroll p-4 md:p-12 md:pt-0 pt-0 grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 gap-4 md:gap-10">
        {pathologies.map(pathology => (
          <Pathology
            name={pathology.name}
            key={pathology._id}
            editMode
            onPathologyDeleted={() => onPathologyRemove(pathology._id)}
          />
        ))}

        <div className="w-full relative">
          <input
            onChange={onNewPathologyChange}
            value={newPathology}
            type="text"
            aria-label="Nom de la pathologie"
            placeholder="Nom de la pathologie"
            className="w-full h-10 flex items-center pl-4 pr-4 rounded-full shadow-md focus:shadow-outline focus:outline-none focus:bg-purple-800 focus:text-white overflow-hidden"
          />
          <button
            onClick={onNewPathologyCreate}
            type="button"
            aria-label="Ajouter une nouvelle pathologie"
            title="Ajouter une nouvelle pathologie"
            className="absolute right-2 top-1/2 transform -translate-y-1/2 p-1 bg-gray-100 rounded-full active:bg-gray-300 focus:outline-none focus:shadow-outline"
          >
            <PlusIcon />
          </button>
        </div>
      </div>
    </div>
  )
}
ManagePathologies.propTypes = {}
export default ManagePathologies
