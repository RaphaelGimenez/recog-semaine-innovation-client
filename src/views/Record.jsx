import React from "react"
import {
  useRouteMatch,
  Switch,
  Route,
  Redirect,
  useHistory,
} from "react-router-dom"
import { nanoid } from "nanoid"
import Recorder from "../components/Recorder"
import PathologySelector from "../components/PathologySelector"
import useDebounce from "../hooks/useDebounce"
import {
  HOST_URI,
  RECORDINGS_ENDPOINT,
  PATHOLOGIES_ENDPOINT,
} from "../env/environnement"

const Record = () => {
  const [pathology, setPathology] = React.useState("")
  const [searchTerm, setSearchTerm] = React.useState("")
  const [pathologies, setPathologies] = React.useState([])
  const debouncedSearchTerm = useDebounce(searchTerm, 300)
  const { path } = useRouteMatch()
  const history = useHistory()
  const PATHOLOGIES_URL = `${HOST_URI}/${PATHOLOGIES_ENDPOINT}`
  const RECORDINGS_URL = `${HOST_URI}/${RECORDINGS_ENDPOINT}`

  React.useEffect(() => {
    const searchUrl = new URL(PATHOLOGIES_URL)
    if (searchTerm) {
      searchUrl.searchParams.append("name", searchTerm)
    }
    fetch(searchUrl, {
      method: "get",
    })
      .then(resp => resp.json())
      .then(res => {
        setPathologies(res)
      })
  }, [debouncedSearchTerm])

  const onSearchChange = e => {
    const { value } = e.target
    setSearchTerm(value)
  }

  const onPathologySelected = async p => {
    if (typeof p !== "string") {
      const searchUrl = new URL(PATHOLOGIES_URL)
      const res = await fetch(searchUrl, {
        method: "post",
        body: JSON.stringify({ name: searchTerm, valid: false }),
        headers: { "Content-Type": "application/json" },
      })
      const { name } = await res.json()
      setPathology(name)
    } else {
      setPathology(p)
    }

    history.push("/record/record")
  }

  const onUpload = records => {
    const formData = new FormData()
    formData.append("uid", nanoid())
    formData.append("pathology", pathology)
    records.forEach(record => {
      formData.append("files", record.audioBlob, `${record.id}.mp3`)
    })
    fetch(new URL(RECORDINGS_URL), {
      method: "post",
      body: formData,
    }).then(resp => resp.json())
    history.push("/record")
  }

  return (
    <div>
      <Switch>
        <Route exact path={`${path}/pathology`}>
          <PathologySelector
            pathologies={pathologies}
            onPathologySelected={onPathologySelected}
            onSearchChange={onSearchChange}
            searchTerm={searchTerm}
          />
        </Route>
        <Route exact path={`${path}/record`}>
          <Recorder onUpload={onUpload} />
        </Route>
        <Redirect from={path} to={`${path}/pathology`} />
      </Switch>
    </div>
  )
}

export default Record
