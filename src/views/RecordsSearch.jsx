import React from "react"
import DayPicker, { DateUtils } from "react-day-picker"
import dayjs from "dayjs"
import SearchBar from "../components/SearchBar"
import useDebounce from "../hooks/useDebounce"
import RecordItem from "../components/RecordItem"
import DownloadIcon from "../icons/DownloadIcon"
import CloseIcon from "../icons/CloseIcon"
import ListIcon from "../icons/ListIcon"
import "react-day-picker/lib/style.css"
import "./DayPicker.css"
import {
  HOST_URI,
  RECORDINGS_ENDPOINT,
  FILES_ENDPOINT,
} from "../env/environnement"

const RecordsSearch = () => {
  const [searchQuery, setSearchQuery] = React.useState({ order: "asc" })
  const [records, setRecords] = React.useState([])
  const [selectedRecords, setSelectedRecords] = React.useState([])
  const [dates, setDates] = React.useState({ from: undefined, to: undefined })
  const [showDatePicker, setShowDatePicker] = React.useState(false)
  const [listMode, setListMode] = React.useState(true)
  const [numberOfMonths, setNumberOfMonths] = React.useState()
  const debouncedSearchQuery = useDebounce(searchQuery, 300)
  const RECORDINGS_URL = `${HOST_URI}/${RECORDINGS_ENDPOINT}`
  const DOWNLOAD_URL = `${HOST_URI}/${FILES_ENDPOINT.download}`

  React.useEffect(() => {
    if (!numberOfMonths) {
      const { innerWidth } = window
      if (innerWidth >= 1280) {
        setNumberOfMonths(4)
      } else if (innerWidth >= 1024) {
        setNumberOfMonths(3)
      } else if (innerWidth >= 768) {
        setNumberOfMonths(2)
      } else {
        setNumberOfMonths(1)
      }
    }
    const newQuery = new URL(RECORDINGS_URL)
    Object.keys(searchQuery).forEach(key => {
      if (searchQuery[key]) {
        newQuery.searchParams.append(key, searchQuery[key])
      }
    })
    fetch(newQuery, {
      method: "get",
    })
      .then(resp => resp.json())
      .then(res => {
        setRecords(res)
      })
  }, [debouncedSearchQuery])

  const onSearchChange = e => {
    const { value } = e.target

    setSearchQuery({ ...searchQuery, pathology: value })
  }

  const onSwitchOrder = () => {
    const { order, ...rest } = searchQuery
    const nextSearchQuery = {
      ...rest,
      order: order === "asc" ? "desc" : "asc",
    }
    setSearchQuery(nextSearchQuery)
  }

  const computeRange = ({ from, to }) => {
    return {
      from: from ? dayjs(from).startOf("day").toDate() : undefined,
      to: to ? dayjs(to).endOf("day").toDate() : undefined,
    }
  }

  const handleDayChange = day => {
    const range = computeRange(DateUtils.addDayToRange(day, dates))
    setDates(range)
    setSearchQuery({
      ...searchQuery,
      startDate: range.from ? dayjs(range.from).valueOf() : undefined,
      endDate: range.to ? dayjs(range.to).valueOf() : undefined,
    })
  }

  const resetDates = () => {
    setDates({})
    setSearchQuery({
      ...searchQuery,
      startDate: undefined,
      endDate: undefined,
    })
    setShowDatePicker(false)
  }

  const downloadQuery = _id => {
    const downloadUrl = new URL(DOWNLOAD_URL)
    if (typeof _id === "string") {
      downloadUrl.searchParams.append("ids", _id)
    } else if (selectedRecords.length && !listMode) {
      downloadUrl.searchParams.append(
        "ids",
        selectedRecords.map(r => r._id)
      )
    } else {
      Object.keys(searchQuery).forEach(key => {
        if (searchQuery[key]) {
          downloadUrl.searchParams.append(key, searchQuery[key])
        }
      })
    }
    window.open(downloadUrl, "_blank")
  }

  const handleRemoveRecord = id => {
    const newQuery = new URL(`${RECORDINGS_URL}/${id}`)
    fetch(newQuery, {
      method: "delete",
    })
      .then(resp => resp.json())
      .then(res => {
        if (res.status === "success") {
          setRecords(records.filter(f => f._id !== id))
        }
      })
  }

  const selectRecord = id => {
    const filteredSelected = selectedRecords.filter(r => r._id === id)

    if (filteredSelected.length > 0) {
      setSelectedRecords(selectedRecords.filter(r => r._id !== id))
    } else {
      setSelectedRecords([
        ...selectedRecords,
        records.filter(r => r._id === id)[0],
      ])
    }
  }

  const DateBtnText = () => {
    const text = []
    if (dates.from || dates.to) {
      if (dates.from) {
        text.push(dayjs(dates.from).format("DD/MM/YY"))
      }
      if (dates.to) {
        text.push(dayjs(dates.to).format("DD/MM/YY"))
      }
    } else {
      text.push("dates")
    }

    return text.join(" - ")
  }

  const resetDatesButton = dates.from ? (
    <button
      type="button"
      onClick={resetDates}
      className="bg-white shadow-md rounded-full py-1 px-1"
    >
      <CloseIcon width={16} height={16} />
    </button>
  ) : null

  return (
    <div>
      <h1 className="text-white text-2xl p-4 pb-0 md:text-4xl md:p-12 md:pb-0 font-bold">
        Gérer les enregistrements
      </h1>
      <div className="mb-4 md:mb-8 p-4 md:p-12 md:pb-0 pb-0 md:flex md:flex-wrap">
        <SearchBar
          className="mb-4 md:max-w-sm md:mr-8"
          placeholder="Rechercher par pathologie"
          label="Rechercher par pathologie"
          onChange={onSearchChange}
        />
        <div className="mb-4  md:mr-8 flex items-center md:inline-flex">
          <button
            onClick={onSwitchOrder}
            type="button"
            aria-label={`${
              searchQuery.order === "asc"
                ? "Les plus récents"
                : "Les plus anciens"
            } en premier`}
            className="border border-white rounded-full py-1 px-2 md:py-2 md:px-4 bg-white text-purple-600 text-sm md:text-base mr-4"
          >
            {searchQuery.order === "asc"
              ? "Les plus récents"
              : "Les plus anciens"}
          </button>
          <button
            type="button"
            className={`border border-white rounded-full py-1 px-2 md:py-2 md:px-4 text-sm md:text-base mr-2 md:mr-0 ${
              dates.from || dates.to
                ? "bg-white text-purple-600"
                : "text-white bg-transparent"
            }`}
            title="Ouvrir le sélecteur de dates"
            aria-label="Ouvrir le sélecteur de dates"
            onClick={() => setShowDatePicker(!showDatePicker)}
          >
            <DateBtnText />
          </button>
          {resetDatesButton}
        </div>

        <div className="flex mb-4 md:inline-flex md:items-center">
          <button
            className="h-10 py-2 px-4 bg-purple-800 text-white shadow-md rounded-full mr-4"
            type="button"
            title={
              listMode ? "Passer en mode sélection" : "Passer en mode liste"
            }
            aria-label={
              listMode ? "Passer en mode sélection" : "Passer en mode liste"
            }
            onClick={() => setListMode(!listMode)}
          >
            <ListIcon />
          </button>
          <button
            className="h-10 py-2 max-w-xs px-4 bg-purple-800 text-white shadow-md rounded-full flex items-center w-full justify-center"
            type="button"
            aria-label="télécharger la sélection"
            title="télécharger la sélection"
            onClick={downloadQuery}
          >
            <DownloadIcon className="mr-2" /> télécharger la sélection
          </button>
        </div>
        {showDatePicker ? (
          <DayPicker
            className={`Selectable w-full bg-white rounded-xlg overflow-hidden ${
              showDatePicker ? "max-h-screen mb-4" : "h-0 mb-0 absolute hidden"
            }`}
            numberOfMonths={numberOfMonths}
            selectedDays={[dates.from, { from: dates.from, to: dates.to }]}
            modifiers={{ start: dates.from, end: dates.to }}
            onDayClick={handleDayChange}
            firstDayOfWeek={1}
          />
        ) : null}
      </div>
      <div className="h-full overflow-y-scroll p-4 md:p-12 md:pt-0 pt-0 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 md:gap-10">
        {records.map(r => (
          <RecordItem
            key={r._id}
            date={r.date}
            pathology={r.pathology}
            records={r.src}
            listMode={listMode}
            downloadRecords={() => downloadQuery(r._id)}
            removeRecord={() => handleRemoveRecord(r._id)}
            selectRecord={() => selectRecord(r._id)}
            checked={
              !!selectedRecords.filter(selected => r._id === selected._id)
                .length
            }
          />
        ))}
      </div>
    </div>
  )
}

RecordsSearch.propTypes = {}

export default RecordsSearch
