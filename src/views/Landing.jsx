import React from "react"
import { Link } from "react-router-dom"
import LogoText from "../assets/logo-text.svg"
import LandingIlustration from "../assets/landing-illustration.svg"

const Landing = () => {
  const hFull = {
    height: "calc(100vh - 50px)",
  }
  return (
    <>
      <header className="flex justify-between absolute w-full pt-4 pl-4 sm:p-20 sm:pt-12 z-10">
        <img src={LogoText} alt="Recog logo with text" />
        <Link
          to="/record"
          role="button"
          className="bg-transparent rounded-full p-2 pr-4 pl-4 text-purple-600 border-purple-600 border-2 sm:block hidden focus:outline-none focus:shadow-outline"
        >
          Donnez votre toux
        </Link>
      </header>
      <main>
        <div
          style={hFull}
          className="flex flex-col lg:flex-row justify-center lg:justify-between items-center text-center lg:text-left p-4 sm:p-20 pt-0 pb-0 transform translate-y-10"
        >
          <div className="max-w-4xl  mb-8 lg:mb-0 lg:mr-8">
            <h1 className="text-2xl sm:text-4xl font-bold mb-4">
              Enregistrez vous tousser et apprenez à nos algorithmes la voix des
              maladies
            </h1>
            <p className="mb-4 text-base sm:text-lg">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Viverra
              volutpat dolor arcu venenatis quis imperdiet. Est velit neque
              tristique tempus ipsum pretium justo, ut. Dolor purus arcu
              condimentum tellus.
            </p>
            <Link
              to="/record"
              role="button"
              className="bg-purple-600 text-lg font-bold rounded-full p-2 pr-4 pl-4 text-white border-purple-600 border-2 
              shadow-lg focus:outline-none focus:shadow-outline"
            >
              Donnez votre toux à la science
            </Link>
          </div>
          <div className="lg:w-2/4 md:w-3/5 hidden md:block max-w-lg flex-shrink-0">
            <img src={LandingIlustration} alt="" />
          </div>
        </div>
        <div className="grid grid-cols-1 lg:grid-cols-3 gap-4 p-4 sm:p-20 pt-0 sm:pt-0">
          <div className="text-center bg-purple-900 text-white rounded-xlg p-4">
            <h2 className="text-lg font-semibold md:text-xl md:font-bold mb-4">
              Connectez vous
            </h2>
            <p className="text-justify">
              Lors de la connexion, nous utiliserons votre email pour vous
              identifier. Vous pouvez si vous le souhaitez vous enregistrer
              anonymement. Dans ce dernier cas, il vous sera impossible de gérer
              vos données ultérieurement.
            </p>
          </div>
          <div className="text-center bg-purple-900 text-white rounded-xlg p-4">
            <h2 className="text-lg font-semibold md:text-xl md:font-bold mb-4">
              Entrez votre maladie
            </h2>
            <p className="text-justify">
              Sélectionnez votre pathologie dans une liste pré établie.
              Attention, votre maladie n&apos;est pas présente dans la liste,
              elle sera approuvée ou non par un administrateur
            </p>
          </div>
          <div className="text-center bg-purple-900 text-white rounded-xlg p-4">
            <h2 className="text-lg font-semibold md:text-xl md:font-bold mb-4">
              Effectuez 3 enregistrements
            </h2>
            <p className="text-justify">
              Laissez vous guider par l&apos;interface afin de vous enregistrer
              et valider vos enregistrements.
            </p>
          </div>
        </div>
      </main>
      <footer className="w-full h-20 flex items-center justify-center text-lg bg-purple-600 text-white">
        <p>
          Créé par :{" "}
          <a
            href="https://ragz.dev"
            target="_blank"
            rel="noreferrer"
            className="text-white border-b-2 border-white hover:text-purple-300 hover:border-purple-300 visited:text-orange-400 visited:border-orange-400"
          >
            Raphael Gimenez
          </a>
        </p>
      </footer>
    </>
  )
}

export default Landing
