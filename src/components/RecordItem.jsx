import React from "react"
import PropTypes from "prop-types"
import * as dayjs from "dayjs"
import InfoIcon from "../icons/InfoIcon"
import DownloadIcon from "../icons/DownloadIcon"
import RemoveIcon from "../icons/RemoveIcon"
import AudioPlayer from "./AudioPlayer"
import { HOST_URI, FILES_ENDPOINT } from "../env/environnement"

const RecordItem = ({
  date,
  pathology,
  records,
  listMode,
  downloadRecords,
  removeRecord,
  selectRecord,
  checked,
}) => {
  const [expanded, setExpanded] = React.useState(false)
  const expandedClasses = "rounded-xlg h-auto pt-4"
  const compactClasses = "h-10 rounded-full flex items-center"

  const recordsItems = expanded ? (
    <div>
      {records.map((record, index) => (
        <AudioPlayer
          key={record}
          audio={new Audio(`${HOST_URI}/${FILES_ENDPOINT.base}/${record}`)}
          id={index}
          valid
        />
      ))}
    </div>
  ) : null

  return (
    <div className="relative">
      {!listMode ? (
        <div
          className="rounded-md border-2 border-white h-4 md:h-5 w-4 md:w-5 absolute left-0 top-1/2 cursor-pointer 
          transform -translate-y-1/2 flex items-center justify-center focus:outline-none focus:shadow-outline"
          tabIndex={0}
          role="checkbox"
          aria-checked={checked}
          aria-label="select"
          onClick={selectRecord}
          onKeyPress={selectRecord}
        >
          <div
            className={`h-2 md:w-3 w-2 md:h-3 bg-white rounded-full ${
              checked ? "display" : "hidden"
            }`}
          />
        </div>
      ) : null}
      <div
        className={`bg-white pl-4 pr-4 transition duration-100 ease
            shadow-md relative overflow-hidden justify-between ${
              !listMode ? "transform translate-x-8" : ""
            } ${expanded ? expandedClasses : compactClasses}`}
      >
        <p className={!expanded ? "w-1/2 truncate" : "text-lg"}>
          {dayjs(date).format("DD/MM/YY")} - <i>{pathology}</i>
        </p>
        <div>
          <button
            type="button"
            className="p-2"
            aria-label="Ouvrir les informations"
            title="Ouvrir les informations"
            onClick={() => setExpanded(!expanded)}
          >
            <InfoIcon />
          </button>
          <button
            type="button"
            className="p-2"
            aria-label="Supprimer l'enregistrement"
            title="Supprimer l'enregistrement"
            onClick={removeRecord}
          >
            <RemoveIcon />
          </button>
          <button
            type="button"
            className="p-2"
            aria-label="Télécharger l'enregistrement"
            title="Télécharger l'enregistrement"
            onClick={downloadRecords}
          >
            <DownloadIcon />
          </button>
        </div>

        {recordsItems}
      </div>
    </div>
  )
}

RecordItem.propTypes = {
  date: PropTypes.number.isRequired,
  downloadRecords: PropTypes.func.isRequired,
  removeRecord: PropTypes.func.isRequired,
  selectRecord: PropTypes.func.isRequired,
  pathology: PropTypes.string.isRequired,
  records: PropTypes.arrayOf(PropTypes.string).isRequired,
  listMode: PropTypes.bool.isRequired,
  checked: PropTypes.bool.isRequired,
}

export default RecordItem
