import React from "react"
import PropTypes from "prop-types"
import PlayIcon from "../icons/PlayIcon"
import StopIcon from "../icons/StopIcon"
import RemoveIcon from "../icons/RemoveIcon"

const AudioPlayer = ({ audio, valid, onRemoveRecord, progress, id }) => {
  const [playing, setPlaying] = React.useState(false)

  const play = () => {
    audio.load()
    audio.play()
    setPlaying(true)
  }

  const stop = () => {
    audio.pause()
    setPlaying(false)
  }

  const playButton = playing ? (
    <button
      disabled={!valid}
      type="button"
      onClick={stop}
      title="Arrêter l'audio"
      aria-label="Arrêter l'audio"
      className="bg-gray-100 rounded-full active:bg-gray-300 focus:outline-none focus:shadow-outline p-1 mr-2 disabled:opacity-50"
    >
      <StopIcon label="Arrêter l'audio" />
    </button>
  ) : (
    <button
      disabled={!valid}
      type="button"
      onClick={play}
      title="Lancer l'audio"
      aria-label="Lancer l'audio"
      className="bg-gray-100 rounded-full active:bg-gray-300 focus:outline-none focus:shadow-outline p-1 mr-2 disabled:opacity-50"
    >
      <PlayIcon label="Lancer l'audio" />
    </button>
  )

  return (
    <div
      className="h-10 bg-white flex items-center pl-4 pr-4 mb-4 rounded-full shadow-md relative overflow-hidden w-full"
      role="region"
      aria-label={`Audio player ${id + 1}`}
    >
      {playButton}

      {onRemoveRecord ? (
        <button
          disabled={!valid}
          type="button"
          onClick={onRemoveRecord}
          title="Supprimer l'audio"
          aria-label="Supprimer l'audio"
          className="bg-gray-100 rounded-full active:bg-gray-300 focus:outline-none focus:shadow-outline p-1 disabled:opacity-50"
        >
          <RemoveIcon label="Supprimer l'audio" />
        </button>
      ) : null}
      <div
        className={`absolute h-full bg-purple-800 -right-2 transition-all duration-100 ease-linear transform rotate-180 shadow-md ${
          valid ? "hidden" : "block"
        }`}
        style={{ width: `calc(${100 - progress}% + 1rem)` }}
      />
    </div>
  )
}

AudioPlayer.defaultProps = {
  audio: undefined,
  valid: false,
  onRemoveRecord: undefined,
  progress: 0,
}

AudioPlayer.propTypes = {
  audio: PropTypes.instanceOf(Audio),
  valid: PropTypes.bool,
  onRemoveRecord: PropTypes.func,
  progress: PropTypes.number,
  id: PropTypes.number.isRequired,
}

export default AudioPlayer
