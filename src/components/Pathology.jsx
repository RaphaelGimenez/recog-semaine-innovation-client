import React from "react"
import PropTypes from "prop-types"
import RemoveIcon from "../icons/RemoveIcon"

const Pathology = ({
  name,
  editMode,
  onPathologySelected,
  onPathologyDeleted,
}) => {
  const accessibiliyAttr = {
    tabIndex: 0,
    onKeyPress: onPathologySelected,
    onClick: onPathologySelected,
  }

  return (
    <label
      className={`h-10 bg-white flex-shrink-0 flex items-center pl-4 pr-4 rounded-full shadow-md relative ${
        !editMode
          ? "focus:shadow-outline focus:outline-none focus:bg-purple-800 focus:text-white overflow-hidden"
          : ""
      }`}
      htmlFor={name}
      {...(!editMode ? accessibiliyAttr : {})}
    >
      {!editMode ? (
        <input
          type="radio"
          id={name}
          name="pathologies"
          className="h-4 w-4 border border-purple-600 block mr-4 rounded-full flex-shrink-0"
        />
      ) : null}
      <span className="whitespace-no-wrap">{name}</span>
      {editMode ? (
        <button
          type="button"
          onClick={onPathologyDeleted}
          aria-label={`Supprimer la pathologie ${name}`}
          title={`Supprimer la pathologie ${name}`}
          className="absolute right-2 top-1/2 transform -translate-y-1/2 p-1 bg-gray-100 rounded-full active:bg-gray-300 focus:outline-none focus:shadow-outline"
        >
          <RemoveIcon />
        </button>
      ) : null}
    </label>
  )
}

Pathology.defaultProps = {
  onPathologySelected: undefined,
  onPathologyDeleted: undefined,
  editMode: false,
}

Pathology.propTypes = {
  name: PropTypes.string.isRequired,
  editMode: PropTypes.bool,
  onPathologySelected: PropTypes.func,
  onPathologyDeleted: PropTypes.func,
}

export default Pathology
