import React from "react"
import PropTypes from "prop-types"
import SearchBar from "./SearchBar"
import Pathology from "./Pathology"

const PathologySelector = ({
  pathologies,
  onPathologySelected,
  onSearchChange,
  searchTerm,
}) => {
  return (
    <div className="flex flex-col items-stretch h-screen">
      <header className="h-24 md:h-40 flex items-center justify-center md:p-12 md:justify-start flex-shrink-0">
        <h1 className="text-xl md:text-4xl font-bold">
          Sélectionnez votre pathologie
        </h1>
      </header>
      <main className="bg-purple-600 p-4 md:p-12 h-full overflow-y-scroll">
        <SearchBar
          onChange={onSearchChange}
          label="Rechercher une pathologie"
          placeholder="Rechercher une pathologie"
          className="mb-4 md:mb-12 max-w-sm"
        />
        <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 gap-4 md:gap-8">
          {pathologies.length === 0 ? (
            <Pathology
              name={searchTerm}
              onPathologySelected={onPathologySelected}
            />
          ) : null}
          {pathologies.map(p => (
            <Pathology
              name={p.name}
              key={p._id}
              onPathologySelected={() => onPathologySelected(p.name)}
            />
          ))}
        </div>
      </main>
    </div>
  )
}

PathologySelector.propTypes = {
  pathologies: PropTypes.arrayOf(
    PropTypes.shape({
      valid: PropTypes.bool,
      name: PropTypes.string,
      _id: PropTypes.string,
    })
  ).isRequired,
  onPathologySelected: PropTypes.func.isRequired,
  searchTerm: PropTypes.string.isRequired,
  onSearchChange: PropTypes.func.isRequired,
}

export default PathologySelector
