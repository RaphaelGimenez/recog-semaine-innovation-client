# Recog Client

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

```bash
# Runs the app in the development mode.
$ yarn start

# Launches the test runner in the interactive watch mode
$ yarn test

# Builds the app for production to the `build` folder
$ yarn build

# Note: this is a one-way operation. Once you `eject`, you can’t go back!
$ yarn eject
```

# Server config and deployment

### Description

This app is meant to get deployed with Dokku. You can easly create a server with Dokku preinstalled using Digital Ocean One click apps.

### Deployment

```bash
# HOST

$ dokku apps:create <app-name>

$ dokku buildpacks:add <app-name> https://github.com/mars/create-react-app-buildpack.git

# LOCAL

$ cd <app-name>

$ git remote add dokku dokku@<hostname_or_ip>:<app-name>

$ git push dokku master
```

### Domain config

Create your wanted subdomain and add a new A record pointing to the server ipv4

### HTTPS Config

```bash
$ sudo dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git

$ sudo dokku plugin:update letsencrypt

$ dokku config:set --no-restart <app-name> DOKKU_LETSENCRYPT_EMAIL=<your_email>

$ dokku letsencrypt <app-name>
```
